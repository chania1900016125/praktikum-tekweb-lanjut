import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.Component.html',
  styleUrls: ['./app.Component.scss']
})
export class AppComponent {
  title = 'myAngular';
}